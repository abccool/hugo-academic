# 该项目已经解决自定义主题中gitlab不支持高版本的hugo的问题
![academic](/static/img/headers/bubbles-wide.jpg)

## 将该项目fork派生以后，请打开 .gitlab-ci.yml文件，查看其中的内容，并按照说明修改。

### .gitlab-ci.yml文件内容如下：

```
# This file is a template, and might need editing before it works on your project.
# Full project: https://gitlab.com/pages/hugo
#image: publysher/hugo:latest
image: registry.gitlab.com/pages/hugo:latest
#将默认生成的image前面加上#注释掉，改为上面hugo项目提供的即可，其中包含了较新版本的HUGO程序
# 所有可用的hugo版本请查看这里: https://gitlab.com/pages/hugo/container_registry

pages:
  script:
  - hugo -b https://abccool.gitlab.io/hugo-academic
  artifacts:
    paths:
    - public
  only:
  - master
  
test:
  script:
  - hugo -b https://abccool.gitlab.io/hugo-academic
  except:
  - master
#说明，使用时候请将hugo -b https://abccool.gitlab.io/hugo-academic这条命令中的网址全部改为你的静态页网址地址
#一般说来是https://用户名.gitlab.io/项目名这样的格式，或者你也可以先删除-b https://abccool.gitlab.io/hugo-academic
#然后先保存一次触发CI，运行通过后得到地址，复制过来将-b后面的网址替换为你自己的，注意和-b之间有一个空格

```